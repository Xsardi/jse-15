package ru.t1.tbobkov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supproted...");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Argument ''" + argument + "'' not supported...");
    }

}
