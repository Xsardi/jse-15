package ru.t1.tbobkov.tm.controller;

import ru.t1.tbobkov.tm.api.controller.ICommandController;
import ru.t1.tbobkov.tm.api.service.ICommandService;
import ru.t1.tbobkov.tm.model.Command;

import static ru.t1.tbobkov.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showVersion() {
        System.out.println("[version]");
        System.out.println("1.15");
    }

    @Override
    public void showWelcome() {
        System.out.println("*** TASK MANAGER ***");
    }

    @Override
    public void showAbout() {
        System.out.println("[about]");
        System.out.println("Name: Timur Bobkov");
        System.out.println("E-mail: tbobkov@t1-consulting.ru");
    }

    @Override
    public void showHelp() {
        System.out.println("[help]");
        for (Command command : commandService.getCommands()) System.out.println(command);
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorsCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorsCount);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("MAXIMUM MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    @Override
    public void showArgumentError() {
        System.out.println("[error]");
        System.out.println("incorrect argument, use '-h' to see the list of commands and arguments");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.out.println("[error]");
        System.out.println("incorrect command, type 'help' to see the list of commands and arguments");
    }

}
