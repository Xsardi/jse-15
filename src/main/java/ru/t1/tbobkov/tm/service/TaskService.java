package ru.t1.tbobkov.tm.service;

import ru.t1.tbobkov.tm.api.repository.ITaskRepository;
import ru.t1.tbobkov.tm.api.service.ITaskService;
import ru.t1.tbobkov.tm.enumerated.Sort;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.exception.entity.TaskNotFoundException;
import ru.t1.tbobkov.tm.exception.field.*;
import ru.t1.tbobkov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository projectRepository) {
        this.taskRepository = projectRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        final Comparator<Task> comparator = sort.getComparator();
        if (comparator == null) return findAll();
        return findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return taskRepository.create(name, description);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public int getSize() {
        return taskRepository.getSize();
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (status == null) throw new StatusIncorrectException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= taskRepository.getSize()) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);

    }
}
